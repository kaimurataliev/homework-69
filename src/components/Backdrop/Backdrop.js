import React from 'react';
import './Backdrop.css';

const Backdrop = props => (
    props.modalShow ? <div className="Backdrop" onClick={props.toggleModal} /> : null
);

export default Backdrop;