import React, { Component } from 'react';
import './Modal.css';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {

    state = {
        name: '',
        email: '',
        error: false
    };

    submitHandler () {
        if(this.state.name === '' || this.state.email === '') {
            return this.setState({error: true});
        }
        this.props.saveOrderToBase(this.state.name, this.state.email);
        this.setState({
            name: '',
            email: '',
            error: false
        });
        this.props.toggleModal();
    }



    render() {
        return (
            <div>
                <Backdrop modalShow={this.props.modalShow} toggleModal={this.props.toggleModal}/>
                <div
                    className="Modal"
                    style={{
                        transform: this.props.modalShow ? 'translateY(0)' : 'translateY(-100vh)',
                        opacity: this.props.modalShow ? '1' : '0'
                    }}
                >

                    {this.state.error && <h1>Fill all fields</h1>}
                    <form action="#">
                        <input
                            type="text"
                            placeholder="Enter your name"
                            value={this.state.name}
                            onChange={(e) => this.setState({name: e.target.value})}
                        />
                        <input
                            type="email"
                            placeholder="Enter your email"
                            value={this.state.email}
                            onChange={(e) => this.setState({email: e.target.value})}
                        />
                        <button onClick={this.submitHandler.bind(this)}>Place order</button>
                    </form>

                </div>
            </div>
        )
    }
}


export default Modal;