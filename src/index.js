import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';

import modalReducer from "./store/modalReducer";
import cartReducer from './store/cartReducer';
import menuReducer from './store/menuReducer';
import MainPage from './containers/MainPage/MainPage';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    cart: cartReducer,
    menu: menuReducer,
    modal: modalReducer
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

const mainPage = (
    <Provider store={store}>
            <MainPage/>
    </Provider>
);

ReactDOM.render(mainPage, document.getElementById('root'));
registerServiceWorker();
