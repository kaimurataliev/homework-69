import {TOGGLE_MODAL} from './actions';


const initiallState = {
    show: false
};

const modalReducer = (state = initiallState, action) => {
    switch(action.type) {
        case TOGGLE_MODAL:
            return {...state, show: !state.show};
        default:
            return state;
    }
};

export default modalReducer;