import { ADD_TO_CART } from './actions';


const initialState = {
    orders: {}
};

const cartReducer = (state = initialState, action) => {
    switch(action.type) {

        case ADD_TO_CART:
            let order;
            if (state.orders[action.order.name]) {
                order = {...action.order, amount: ++ state.orders[action.order.name].amount}
            } else {
                order = {...action.order, amount: 1}
            }
            const orders = {...state.orders, [action.order.name]: order};
            return {
                ...state,
                orders
            };
        default: return state;
            // return {...state, orders: {...state.orders, [action.order.name]: action.order}};
    }
};

export default cartReducer;