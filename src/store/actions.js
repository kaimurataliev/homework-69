import axios from '../axios-orders';

export const ADD_TO_CART = "ADD_TO_CART";
export const FETCH_MENU_SUCCESS = "FETCH_MENU_SUCCESS";
export const FETCH_REQUEST = "FETCH_REQUEST";
export const FETCH_ERROR = "FETCH_ERROR";
export const PLACE_ORDER = "PLACE_ORDER";
export const TOGGLE_MODAL = "TOGGLE_MODAL";


export const toggleModal = () => {
    return {type: TOGGLE_MODAL};
};

export const fetchMenuRequest = () => {
    return {type: FETCH_REQUEST}
};

export const fetchMenuSuccess = (data) => {
    return {type: FETCH_MENU_SUCCESS, data}
};

export const fetchError = () => {
    return {type: FETCH_ERROR}
};

export const addToCart = (order) => {
    return {type: ADD_TO_CART, order}
};

export const placeOrder = (order) => {
    return {type: PLACE_ORDER, order}
};

export const getAllMenu = () => {
    return (dispatch) => {
        dispatch(fetchMenuRequest());

        axios.get('./menu.json')
            .then(response => {
                dispatch(fetchMenuSuccess(response.data));
            }, error => {
                dispatch(fetchError(error));
            })
    }
};

export const saveOrderToBase = (name, email) => {
    return (dispatch, getState) => {
        dispatch(fetchMenuRequest());

        axios.post('./orders.json', {
            order: getState().cart.orders,
            name: name,
            email: email
        });
    }
};