import * as actions from './actions';

const initialState = {
    menu: {},
    isLoading: false,
    error: false
};

const menuReducer = (state = initialState, action) => {
    switch(action.type) {

        case actions.FETCH_REQUEST:
            return {...state, isLoading: true};

        case actions.FETCH_MENU_SUCCESS:
            return {...state, menu: action.data};

        case actions.FETCH_ERROR:
            return {...state, error: true};

        default:
            return state;
    }
};


export default menuReducer;