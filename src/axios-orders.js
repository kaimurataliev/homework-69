import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://homework-69.firebaseio.com'
});


export default instance;