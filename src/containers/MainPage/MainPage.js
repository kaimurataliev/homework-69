import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {addToCart, getAllMenu, saveOrderToBase, toggleModal} from "../../store/actions";

import Menu from '../Menu/Menu';
import Cart from '../Cart/Cart';
import './MainPage.css';
import Modal from "../../components/Modal/Modal";

class MainPage extends Component {
    render() {
        return (
            <Fragment>
                <div className="main-page">
                    <Menu
                        menu={this.props.menu}
                        getAllMenu={this.props.getAllMenu}
                        addToCart={this.props.addToCart}
                    />

                    <Cart
                        cart={this.props.cart}
                        toggleModal={this.props.toggleModal}
                    />
                </div>
                <Modal modalShow={this.props.modalShow}
                       toggleModal={this.props.toggleModal}
                       cart={this.props.cart}
                       saveOrderToBase={this.props.saveOrderToBase}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        menu: state.menu.menu,
        cart: state.cart.orders,
        modalShow: state.modal.show
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAllMenu: () => dispatch(getAllMenu()),
        addToCart: (order) => dispatch(addToCart(order)),
        saveOrderToBase: (name, email) => dispatch(saveOrderToBase(name, email)),
        toggleModal: () => dispatch(toggleModal())
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(MainPage);