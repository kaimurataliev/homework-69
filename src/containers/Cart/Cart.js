import React, { Component } from 'react';

import './Cart.css';

class Cart extends Component {
    calculatePrice () {
        const cart = this.props.cart;
        let total = 0;

        for (const key in cart) {
            if (!cart.hasOwnProperty(key)) continue;
            const item = cart[key];

            total = total + (item.price * item.amount)
        }

       return total;
    }

    render () {
        const orders = Object.keys(this.props.cart);
        const totalPrice = this.calculatePrice();
        return (
            <div className="cart">
                <h2>Your orders</h2>
                {
                    orders.map((key, index) => {
                        return (
                            <div className="order" key={index}>
                                <h4>Name: {this.props.cart[key].name}</h4>
                                <p>Price: {this.props.cart[key].price} KGZ</p>
                                <p>Amount: {this.props.cart[key].amount}</p>
                            </div>
                        )
                    })
                }
                <button onClick={() => this.props.toggleModal()} disabled={!orders.length}>Place order ({totalPrice}KGZ)</button>
            </div>
        )
    }
}


export default Cart;