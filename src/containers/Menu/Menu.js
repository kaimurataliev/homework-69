import React, { Component } from 'react';

import './Menu.css';


class Menu extends Component {

    componentDidMount() {
        this.props.getAllMenu();
    }

    render() {

        return (
            <div className="menu">
                <h2>List of food</h2>
                {
                    Object.keys(this.props.menu).map((key, index) => {
                        return (
                            <div key={index} className="menu-item">
                                <h4>Name: {this.props.menu[key].name}</h4>
                                <p>Price: {this.props.menu[key].price} KGZ</p>
                                <img src={this.props.menu[key].image} alt="img"/>

                                <button onClick={() => this.props.addToCart({name: this.props.menu[key].name,
                                    price: this.props.menu[key].price})}>Add
                                </button>

                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default Menu;